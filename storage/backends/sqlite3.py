import sqlite3


class Driver:
    conn = None

    def __init__(self, params):
        self.conn = sqlite3.connect(params['database'])
        self.cursor = self.conn.cursor()

    def prepare(self):
        self.cursor.execute("""
          CREATE TABLE IF NOT EXISTS inventory (
            id INTEGER PRIMARY KEY,
            name TEXT NOT NULL,
            state TEXT,
            price_usd REAL,
            trade_area TEXT,
            bot_id INTEGER
          );
        """)
        self.cursor.execute("""
          CREATE TABLE IF NOT EXISTS items (
            id INTEGER PRIMARY KEY,
            name TEXT NOT NULL,
            state TEXT,
            quantity INTEGER,
            normal_price_usd REAL,
            sale_price_usd REAL,
            trade_area TEXT
          );
        """)
        self.conn.commit()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.conn.commit()
        self.cursor.close()
        self.conn.close()
