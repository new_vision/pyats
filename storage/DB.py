import pyats.settings
from storage.constants import *


class DB:
    driver = None

    def __init__(self):
        self.db = pyats.settings.DATABASES['default']
        if self.db['ENGINE'] == DB_MYSQL:
            import storage.backends.mysql as backend
        elif self.db['ENGINE'] == DB_POSTGRES:
            import storage.backends.postgres as backend
        else:
            import storage.backends.sqlite3 as backend

        self.driver = backend.Driver(self.db['PARAMS'])

        self.driver.prepare()
