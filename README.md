# Project Structure #
![project structure](https://image.ibb.co/c3smPb/project_structure.jpg)

# README #

I think that it would be much better if we will have one repository for whole project because we have to create folders that describes project structure and python scripts will have relative import paths in their headers.

For great feature adding we can use branch system. Master branch is our main branch which contain just approved code and if you start writing some Module or even feature (if you want to, ofc), you just have to create another branch with correct and *quick understandable* name and when you decide that it works fine merge it with master branch.

Feel free to discuss about it, I just want to bring some new experience for us (and for me too) like other respectful teams do.

And I want to try [Trello](https://trello.com/), very useful (should be).
