# Settings module (global)
import os
import storage.constants

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

DATABASES = {
    'default': {
        'ENGINE': storage.constants.DB_SQLITE3,
        'PARAMS': {
            'database': 'db.sqlite'
        }
    }
}
