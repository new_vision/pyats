#!/usr/bin/python3
# -*- coding: utf-8 -*-

from currency_refresher import steam_currencies
from pprint import *

steam_curr_getter = steam_currencies.SteamCurrencies()
print("max: ", steam_curr_getter.get_total_count())
lst = steam_curr_getter.get_currencies_list(start=9000, count=1000)

with open("results.txt", "w+") as f:
    f.write(pformat(lst, width=100))
    f.close()

