#!/usr/bin/python3
# -*- coding: utf-8 -*-

from fake_useragent import UserAgent
import requests


class CurrenciesGetter:

    def __init__(self):
        self.session = requests.Session()
        self.session.adapters.DEFAULT_RETRIES = 2
        self.ua = UserAgent()

    def get_currencies_list(self, count):
        raise NotImplementedError("Please Implement this method")

    def get_total_count(self):
        raise NotImplementedError("Please Implement this method")

    def update_db(self):
        pass
