#!/usr/bin/python3
# -*- coding: utf-8 -*-
import time

from currency_refresher.currencies_getter import CurrenciesGetter
from bs4 import BeautifulSoup
from pprint import pprint
import json


class SteamCurrencies(CurrenciesGetter):

    url = "http://steamcommunity.com/market/search/render/?query={query}&start={start}&count={" \
          "count}&search_description=0&sort_column={sort_column}&sort_dir={sort_dir}&appid=730"

    def make_url(self, start=0, count=100, query="", sort_column="quantity", sort_dir="desc"):
        return self.url.format(query=query, start=start, count=count, sort_column=sort_column, sort_dir=sort_dir)

    def get_total_count(self):
        self.session.headers["User-Agent"] = str(self.ua.random)
        r = self.session.get(self.make_url(start=0, count=0))
        response = json.loads(r.content)
        if response is not None and response[u'success'] is True:
            return int(response[u'total_count'])
        else:
            raise Warning("get count error")

    def get_currencies_list(self, start=0, count=-1, attempts=-1):
        """
        Query steam for available currencies
        :param start: the start index to get currencies
        :param count: how many currencies to get (if not used then all currencies will be retrieved
        :param attempts: -1 is infinity
        :return: list of dictionaries if success and empty list if failed
        """
        if count == -1:
            count = self.get_total_count()
        else:
            count = start + count
        iter_count = 100
        attempt = 0
        wait_time = 120
        results = []
        for i in range(start, count, iter_count):
            while True:
                if iter_count > count-i:
                    iter_count = count-i
                self.session.headers["User-Agent"] = str(self.ua.random)

                response = self.session.get(self.make_url(start=i, count=iter_count))

                json_response = json.loads(response.content)

                if json_response is not None and json_response[u'success'] is True:
                    soup = BeautifulSoup(json_response[u'results_html'], 'html.parser')
                    search_results = soup.find_all('div', {'class': 'market_listing_searchresult'})

                    for search_result in search_results:
                        prices = search_result.find('span', {'class': 'market_table_value normal_price'})
                        d = {
                            "name": search_result.find('span', {'class': 'market_listing_item_name'}).text,
                            "quantity": search_result.find('span', {'class': 'market_listing_num_listings_qty'}).text,
                            "normal_price": prices.find('span', {'class': 'normal_price'}).text,
                            "sale_price": prices.find('span', {'class': 'sale_price'}).text
                        }
                        results.append(d)
                    print("Retrieved", i + iter_count - start)
                    break
                else:
                    if attempts == -1 or attempt < attempts:
                        attempt += 1
                        print("Attempt ", attempt, ", Error at index", i, "Status:", response.status_code)
                        time.sleep(wait_time)
                    else:
                        print("Too many attempts! Quitting..")
                        return results
        return results
